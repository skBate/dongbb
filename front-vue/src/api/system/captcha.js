import {jwtServerInstance} from "../index";

//生成验证码图片
export function gen(type) {
  return jwtServerInstance.request({
    url: '/captcha/gen',
    method: 'get',
    params: {type}
  })
}

//校验验证码
export function check(captchaId, data) {
  return jwtServerInstance.request({
    url: '/captcha/check?id=' + captchaId,
    method: 'post',
    data: data
  })
}