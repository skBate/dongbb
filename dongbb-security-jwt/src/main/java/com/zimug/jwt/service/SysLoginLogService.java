package com.zimug.jwt.service;

import com.zimug.jwt.mapper.SysLoginLogMapper;
import com.zimug.jwt.model.SysLoginLog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @创建人 韩晓彤
 * @创建时间 2023/7/12
 * @描述
 **/
@Service
public class SysLoginLogService {
  private static final Logger logger = LoggerFactory.getLogger(SysLoginLogService.class);


  @Resource
  private SysLoginLogMapper sysLoginLogMapper;


  //为避免拖慢登录过程，使用异步方式记录日志
  @Async
  public void add(SysLoginLog sysLoginlog){
    try {
      sysLoginLogMapper.insert(sysLoginlog);
    }catch (Exception e){
      logger.error("记录sysLoginlog出现异常",e);
    }

  }
}
