package com.zimug.jwt.model;


import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;


@ConfigurationProperties(prefix = "zimug.jwt")
public class JwtProperties {

    //是否开启JWT，即注入相关的类对象
    private Boolean enabled;
    //JWT密钥
    private String secret;
    //JWT有效时间
    private Long expiration;
    //前端向后端传递JWT时使用HTTP的header名称
    private String header;
    //允许哪些域对本服务的跨域请求
    private List<String> corsAllowedOrigins;
    //允许哪些HTTP方法跨域
    private List<String> corsAllowedMethods;
    //用户获取JWT令牌发送的用户名参数名称
    private String userParamName = "username";
    //用户获取JWT令牌发送的密码参数名称
    private String pwdParamName = "password";
    //是否关闭csrf跨站攻击防御功能
    private Boolean csrfDisabled = true;
    //是否使用默认的JWTAuthController
    private Boolean useDefaultController = true;
    //开发过程临时开放的URI
    private List<String> devOpeningURI;
    //权限全面开放的接口，不需要JWT令牌就可以访问
    private List<String> permitAllURI;


    public Boolean getEnabled() {
      return enabled;
    }

    public void setEnabled(Boolean enabled) {
      this.enabled = enabled;
    }

    public String getSecret() {
      return secret;
    }

    public void setSecret(String secret) {
      this.secret = secret;
    }

    public Long getExpiration() {
      return expiration;
    }

    public void setExpiration(Long expiration) {
      this.expiration = expiration;
    }

    public String getHeader() {
      return header;
    }

    public void setHeader(String header) {
      this.header = header;
    }

    public List<String> getCorsAllowedOrigins() {
      return corsAllowedOrigins;
    }

    public void setCorsAllowedOrigins(List<String> corsAllowedOrigins) {
      this.corsAllowedOrigins = corsAllowedOrigins;
    }

    public List<String> getCorsAllowedMethods() {
      return corsAllowedMethods;
    }

    public void setCorsAllowedMethods(List<String> corsAllowedMethods) {
      this.corsAllowedMethods = corsAllowedMethods;
    }

    public String getUserParamName() {
      return userParamName;
    }

    public void setUserParamName(String userParamName) {
      this.userParamName = userParamName;
    }

    public String getPwdParamName() {
      return pwdParamName;
    }

    public void setPwdParamName(String pwdParamName) {
      this.pwdParamName = pwdParamName;
    }

    public Boolean getCsrfDisabled() {
      return csrfDisabled;
    }

    public void setCsrfDisabled(Boolean csrfDisabled) {
      this.csrfDisabled = csrfDisabled;
    }

    public Boolean getUseDefaultController() {
      return useDefaultController;
    }

    public void setUseDefaultController(Boolean useDefaultController) {
      this.useDefaultController = useDefaultController;
    }

    public List<String> getDevOpeningURI() {
      return devOpeningURI;
    }

    public void setDevOpeningURI(List<String> devOpeningURI) {
      this.devOpeningURI = devOpeningURI;
    }

    public List<String> getPermitAllURI() {
      return permitAllURI;
    }

    public void setPermitAllURI(List<String> permitAllURI) {
      this.permitAllURI = permitAllURI;
    }
}
