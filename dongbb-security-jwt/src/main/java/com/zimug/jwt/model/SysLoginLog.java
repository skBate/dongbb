package com.zimug.jwt.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;

/**
 * @创建人 韩晓彤
 * @创建时间 2023/7/12
 * @描述 用户登录日志
 **/
public class SysLoginLog {

  @TableId(type = IdType.AUTO)
  private Long id;

  private String username;

  private Boolean isOk;

  private String ipAddr;

  private String message;

  private Date accessTime;

  public SysLoginLog(String username) {
    this.username = username;
    this.accessTime = new Date();
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Boolean getOk() {
    return isOk;
  }

  public void setOk(Boolean ok) {
    isOk = ok;
  }

  public String getIpAddr() {
    return ipAddr;
  }

  public void setIpAddr(String ipAddr) {
    this.ipAddr = ipAddr;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Date getAccessTime() {
    return accessTime;
  }

  public void setAccessTime(Date accessTime) {
    this.accessTime = accessTime;
  }
}
