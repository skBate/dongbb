package com.zimug.jwt.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zimug.jwt.model.SysLoginLog;

/**
 * @创建人 韩晓彤
 * @创建时间 2023/7/12
 * @描述
 **/
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {
}
