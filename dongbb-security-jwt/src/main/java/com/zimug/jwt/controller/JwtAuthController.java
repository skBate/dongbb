package com.zimug.jwt.controller;


import cn.hutool.extra.servlet.ServletUtil;
import com.zimug.commons.exception.AjaxResponse;
import com.zimug.commons.exception.CustomException;
import com.zimug.commons.exception.CustomExceptionType;
import com.zimug.jwt.model.JwtProperties;
import com.zimug.jwt.model.SysLoginLog;
import com.zimug.jwt.service.JwtAuthService;
import com.zimug.jwt.service.SysLoginLogService;
import com.zimug.jwt.utils.JWTConstants;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * JWT获取令牌和刷新令牌接口
 */
@RestController
@ConditionalOnBean({JwtAuthService.class})
@ConditionalOnProperty(name = "zimug.jwt.useDefaultController",
                        havingValue = "true")
public class JwtAuthController {

    @Resource
    private JwtProperties jwtProperties;

    @Resource
    private JwtAuthService jwtAuthService;

    @Resource
    private SysLoginLogService sysLoginLogService;

    /**
     * 使用用户名密码换JWT令牌
     */
    @RequestMapping(value = JWTConstants.CONTROLLER_AUTHENTICATION)
    public AjaxResponse login(@RequestBody Map<String,String> map,
                              HttpServletRequest request){

        String username  = map.get(jwtProperties.getUserParamName());
        String password = map.get(jwtProperties.getPwdParamName());
        SysLoginLog sysLoginLog = new SysLoginLog(username);
        sysLoginLog.setIpAddr(ServletUtil.getClientIP(request));

        if(StringUtils.isEmpty(username)
                || StringUtils.isEmpty(password)){
            sysLoginLog.setOk(false);
            sysLoginLog.setMessage("用户填写用户名密码为空");
            return AjaxResponse.error(
                    CustomExceptionType.USER_INPUT_ERROR,
                    "用户名或者密码不能为空");
        }
        try {
            String token = jwtAuthService.login(username, password,null);
            sysLoginLog.setOk(true);
            sysLoginLog.setMessage("用户登陆成功");
            sysLoginLogService.add(sysLoginLog);
            return AjaxResponse.success(token);
        }catch (CustomException e){
            sysLoginLog.setOk(false);
            sysLoginLog.setMessage(e.getMessage());
            sysLoginLogService.add(sysLoginLog);
            return AjaxResponse.error(e);
        }

    }

    /**
     * 刷新JWT令牌
     */
    @RequestMapping(value = JWTConstants.CONTROLLER_REFRESH)
    public  AjaxResponse refresh(@RequestHeader("${zimug.jwt.header}") String token){
            return AjaxResponse.success(jwtAuthService.refreshToken(token));
    }


    /**
     * 获取用户角色列表接口
     */
    @RequestMapping(value = JWTConstants.CONTROLLER_ROLES)
    public  AjaxResponse roles(
      @RequestHeader("${zimug.jwt.header}") String token){
      return AjaxResponse.success(jwtAuthService.roles(token));
    }

}
