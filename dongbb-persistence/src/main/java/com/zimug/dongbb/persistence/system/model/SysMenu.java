package com.zimug.dongbb.persistence.system.model;

import com.zimug.dongbb.persistence.system.common.BaseColumns;

/**
 * sys_menu
 * @author 字母哥
 */
public class SysMenu extends BaseColumns {
    private Long id;

    /**
     * 父菜单ID
     */
    private Long menuPid;

    /**
     * 当前菜单所有父菜单
     */
    private String menuPids;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 排序
     */
    private Integer menuSort;


    /**
     * 0:不是叶子节点，1:是叶子节点
     */
    private Boolean isLeaf;

    /**
     * 跳转URL
     */
    private String url;

    private String icon;

    /**
     * 菜单层级
     */
    private Integer level;

    /**
     * 0:启用,1:禁用
     */
    private Boolean status;


    /**
     * 0:不隐藏,1:隐藏（是否隐藏菜单，某些页面入口不在菜单上显示）
     */
    private Boolean hidden;


    /**
     * 前端路由组件页面文件import路径
     */
    private String viewImport;


    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public Long getMenuPid() {
      return menuPid;
    }

    public void setMenuPid(Long menuPid) {
      this.menuPid = menuPid;
    }

    public String getMenuPids() {
      return menuPids;
    }

    public void setMenuPids(String menuPids) {
      this.menuPids = menuPids;
    }

    public String getMenuName() {
      return menuName;
    }

    public void setMenuName(String menuName) {
      this.menuName = menuName;
    }

    public Integer getMenuSort() {
      return menuSort;
    }

    public void setMenuSort(Integer menuSort) {
      this.menuSort = menuSort;
    }

    public Boolean getIsLeaf() {
      return isLeaf;
    }

    public void setIsLeaf(Boolean leaf) {
      isLeaf = leaf;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getIcon() {
      return icon;
    }

    public void setIcon(String icon) {
      this.icon = icon;
    }

    public Integer getLevel() {
      return level;
    }

    public void setLevel(Integer level) {
      this.level = level;
    }

    public Boolean getStatus() {
      return status;
    }

    public void setStatus(Boolean status) {
      this.status = status;
    }

    public Boolean getHidden() {
      return hidden;
    }

    public void setHidden(Boolean hidden) {
      this.hidden = hidden;
    }

    public String getViewImport() {
      return viewImport;
    }

    public void setViewImport(String viewImport) {
      this.viewImport = viewImport;
    }
}