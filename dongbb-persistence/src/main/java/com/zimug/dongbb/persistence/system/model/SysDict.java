package com.zimug.dongbb.persistence.system.model;

import com.zimug.dongbb.persistence.system.common.BaseColumns;


public class SysDict extends BaseColumns {

    //
    private Long id;
    //分组名称
    private String groupName;
    //分组编码
    private String groupCode;
    //字典项名称
    private String itemName;
    //字典项Value
    private String itemValue;
    //字典项描述
    private String itemDesc;

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getGroupName() {
      return groupName;
    }

    public void setGroupName(String groupName) {
      this.groupName = groupName;
    }

    public String getGroupCode() {
      return groupCode;
    }

    public void setGroupCode(String groupCode) {
      this.groupCode = groupCode;
    }

    public String getItemName() {
      return itemName;
    }

    public void setItemName(String itemName) {
      this.itemName = itemName;
    }

    public String getItemValue() {
      return itemValue;
    }

    public void setItemValue(String itemValue) {
      this.itemValue = itemValue;
    }

    public String getItemDesc() {
      return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
      this.itemDesc = itemDesc;
    }
}