package com.zimug.dongbb.persistence.system.model;

import com.zimug.dongbb.persistence.system.common.BaseColumns;

/**
 * sys_user表对应的实体类
 * @author 字母哥
 */
public class SysUser extends BaseColumns {
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户所属组织id
     */
    private Long orgId;

    /**
     * 0无效用户，1是有效用户
     */
    private Boolean enabled;

    /**
     * 手机号
     */
    private String phone;

    /**
     * email
     */
    private String email;


    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getUsername() {
      return username;
    }

    public void setUsername(String username) {
      this.username = username;
    }

    public String getPassword() {
      return password;
    }

    public void setPassword(String password) {
      this.password = password;
    }

    public Long getOrgId() {
      return orgId;
    }

    public void setOrgId(Long orgId) {
      this.orgId = orgId;
    }

    public Boolean getEnabled() {
      return enabled;
    }

    public void setEnabled(Boolean enabled) {
      this.enabled = enabled;
    }

    public String getPhone() {
      return phone;
    }

    public void setPhone(String phone) {
      this.phone = phone;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }
}