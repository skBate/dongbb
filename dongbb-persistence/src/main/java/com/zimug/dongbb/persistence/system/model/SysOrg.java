package com.zimug.dongbb.persistence.system.model;

import com.zimug.dongbb.persistence.system.common.BaseColumns;

/**
 * sys_org
 * @author 字母哥
 */
public class SysOrg extends BaseColumns {
    private Long id;

    /**
     * 上级组织编码
     */
    private Long orgPid;

    /**
     * 所有的父节点id
     */
    private String orgPids;

    /**
     * 0:不是叶子节点，1:是叶子节点
     */
    private Boolean isLeaf;

    /**
     * 组织名
     */
    private String orgName;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 邮件
     */
    private String email;

    /**
     * 排序
     */
    private Integer orgSort;

    /**
     * 组织层级
     */
    private Integer level;

    /**
     * 0:启用,1:禁用
     */
    private Boolean status;


    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public Long getOrgPid() {
      return orgPid;
    }

    public void setOrgPid(Long orgPid) {
      this.orgPid = orgPid;
    }

    public String getOrgPids() {
      return orgPids;
    }

    public void setOrgPids(String orgPids) {
      this.orgPids = orgPids;
    }

    public Boolean getIsLeaf() {
      return isLeaf;
    }

    public void setIsLeaf(Boolean leaf) {
      isLeaf = leaf;
    }

    public String getOrgName() {
      return orgName;
    }

    public void setOrgName(String orgName) {
      this.orgName = orgName;
    }

    public String getAddress() {
      return address;
    }

    public void setAddress(String address) {
      this.address = address;
    }

    public String getPhone() {
      return phone;
    }

    public void setPhone(String phone) {
      this.phone = phone;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }

    public Integer getOrgSort() {
      return orgSort;
    }

    public void setOrgSort(Integer orgSort) {
      this.orgSort = orgSort;
    }

    public Integer getLevel() {
      return level;
    }

    public void setLevel(Integer level) {
      this.level = level;
    }

    public Boolean getStatus() {
      return status;
    }

    public void setStatus(Boolean status) {
      this.status = status;
    }
}