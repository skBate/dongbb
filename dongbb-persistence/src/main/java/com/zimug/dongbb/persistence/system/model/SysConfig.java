package com.zimug.dongbb.persistence.system.model;

import com.zimug.dongbb.persistence.system.common.BaseColumns;

/**
 * sys_config
 * @author 字母哥
 */
public class SysConfig extends BaseColumns {
    private Long id;

    /**
     * 参数名称(中文)
     */
    private String paramName;

    /**
     * 参数唯一标识(英文及数字)
     */
    private String paramKey;

    /**
     * 参数值
     */
    private String paramValue;

    /**
     * 参数描述备注
     */
    private String paramDesc;


    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getParamName() {
      return paramName;
    }

    public void setParamName(String paramName) {
      this.paramName = paramName;
    }

    public String getParamKey() {
      return paramKey;
    }

    public void setParamKey(String paramKey) {
      this.paramKey = paramKey;
    }

    public String getParamValue() {
      return paramValue;
    }

    public void setParamValue(String paramValue) {
      this.paramValue = paramValue;
    }

    public String getParamDesc() {
      return paramDesc;
    }

    public void setParamDesc(String paramDesc) {
      this.paramDesc = paramDesc;
    }
}