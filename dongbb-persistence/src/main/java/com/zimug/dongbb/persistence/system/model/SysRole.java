package com.zimug.dongbb.persistence.system.model;

import com.zimug.dongbb.persistence.system.common.BaseColumns;

/**
 * sys_role
 * @author 字母哥
 */
public class SysRole extends BaseColumns {
    private Long id;

    /**
     * 角色名称(汉字)
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String roleDesc;

    /**
     * 角色的英文code.如：ADMIN
     */
    private String roleCode;

    /**
     * 角色顺序
     */
    private Integer roleSort;

    /**
     * 0表示可用
     */
    private Boolean status;


    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getRoleName() {
      return roleName;
    }

    public void setRoleName(String roleName) {
      this.roleName = roleName;
    }

    public String getRoleDesc() {
      return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
      this.roleDesc = roleDesc;
    }

    public String getRoleCode() {
      return roleCode;
    }

    public void setRoleCode(String roleCode) {
      this.roleCode = roleCode;
    }

    public Integer getRoleSort() {
      return roleSort;
    }

    public void setRoleSort(Integer roleSort) {
      this.roleSort = roleSort;
    }

    public Boolean getStatus() {
      return status;
    }

    public void setStatus(Boolean status) {
      this.status = status;
    }
}