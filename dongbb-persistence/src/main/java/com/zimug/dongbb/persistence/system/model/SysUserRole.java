package com.zimug.dongbb.persistence.system.model;

/**
 * sys_user_role
 * @author
 */
public class SysUserRole {

    /**
     * 角色自增id
     */
    private Long roleId;

    /**
     * 用户自增id
     */
    private Long userId;


    public Long getRoleId() {
      return roleId;
    }

    public void setRoleId(Long roleId) {
      this.roleId = roleId;
    }

    public Long getUserId() {
      return userId;
    }

    public void setUserId(Long userId) {
      this.userId = userId;
    }
}