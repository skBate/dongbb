package com.zimug.dongbb.persistence.system.model;

public class SysUserOrg extends SysUser {

  private String orgName;

  public String getOrgName() {
    return orgName;
  }

  public void setOrgName(String orgName) {
    this.orgName = orgName;
  }
}
