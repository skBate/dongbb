package com.zimug.dongbb.persistence.system.model;

/**
 * sys_role_api
 * @author
 */
public class SysRoleApi  {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 接口id
     */
    private Long apiId;


    public Long getRoleId() {
      return roleId;
    }

    public void setRoleId(Long roleId) {
      this.roleId = roleId;
    }

    public Long getApiId() {
      return apiId;
    }

    public void setApiId(Long apiId) {
      this.apiId = apiId;
    }
}