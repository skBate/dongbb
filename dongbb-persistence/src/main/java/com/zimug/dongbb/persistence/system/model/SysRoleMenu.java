package com.zimug.dongbb.persistence.system.model;

/**
 * sys_role_menu
 * @author
 */
public class SysRoleMenu  {

    /**
     * 角色id
     */
    private Long roleId;

    /**
     * 权限id
     */
    private Long menuId;

    public Long getRoleId() {
      return roleId;
    }

    public void setRoleId(Long roleId) {
      this.roleId = roleId;
    }

    public Long getMenuId() {
      return menuId;
    }

    public void setMenuId(Long menuId) {
      this.menuId = menuId;
    }
}