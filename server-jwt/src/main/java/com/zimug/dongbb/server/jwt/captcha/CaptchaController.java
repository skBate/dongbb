package com.zimug.dongbb.server.jwt.captcha;

import cloud.tianai.captcha.common.constant.CaptchaTypeConstant;
import cloud.tianai.captcha.spring.application.ImageCaptchaApplication;
import cloud.tianai.captcha.spring.vo.CaptchaResponse;
import cloud.tianai.captcha.spring.vo.ImageCaptchaVO;
import cloud.tianai.captcha.validator.common.model.dto.ImageCaptchaTrack;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 验证码生成及校验
 */
@RestController
@RequestMapping("/captcha")
public class CaptchaController {


    @Resource
    private ImageCaptchaApplication imageCaptchaApplication;

    //生成验证码图片信息
    @GetMapping("/gen")
    @ResponseBody
    public CaptchaResponse<ImageCaptchaVO> genCaptcha(@RequestParam(value = "type", required = false)String type) {
        if (StringUtils.isBlank(type)) {
            type = CaptchaTypeConstant.SLIDER;
        }
      return imageCaptchaApplication.generateCaptcha(type);
    }

    //验证码校验
    @PostMapping("/check")
    @ResponseBody
    public boolean checkCaptcha(@RequestParam("id") String id,
                                @RequestBody ImageCaptchaTrack imageCaptchaTrack) {
        return imageCaptchaApplication.matching(id, imageCaptchaTrack).isSuccess();
    }
}
